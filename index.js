module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: ["./tsconfig.json"],
  },
  extends: [
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended",
    "plugin:jsx-a11y/recommended",
    "airbnb-typescript",
    "plugin:prettier/recommended",
    "prettier",
  ],
  plugins: ["prettier", "react", "@typescript-eslint", "jsx-a11y"],
  rules: {
    "@typescript-eslint/no-explicit-any": "error",

    "import/order": [
      "error",
      {
        groups: [
          ["builtin", "external"],
          ["parent", "sibling", "index"],
        ],
        "newlines-between": "always",
        alphabetize: {
          order: "asc",
        },
      },
    ],
    "import/prefer-default-export": "off",

    "padding-line-between-statements": [
      "error",
      { blankLine: "always", prev: "*", next: "return" },
      { blankLine: "always", prev: ["const", "let"], next: "*" },
      { blankLine: "any", prev: "const", next: "const" },
      { blankLine: "any", prev: "let", next: "let" },
    ],

    "react/jsx-props-no-spreading": "off",
    "react/no-array-index-key": "off",
    "react/prop-types": "off",
    "react/require-default-props": "off",
  },
  ignorePatterns: ["*.svg", "*.png", "*.jpg"],
};
